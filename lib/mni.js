const Server = require('./server')
const Approval = require('./approval')

const log = require('./log')

module.exports = class MNI {
  constructor (config) {
    const { approval } = config

    // local tcp server, clients can connect to request package installations,
    // the server will handle the approval
    this.server = new Server(config.port || 6780)

    // approver, will handle npm package metadata fetching and
    // (most importantly) sending messages and receiving approvals from discord
    this.approval = new Approval(approval.token, approval)

    this.registerHandlers()
  }

  registerHandlers () {
    this.server.on('installRequest', async (conn, pkg) => {
      log.verbose(`going to submit request ${pkg}`)

      try {
        var approved = await this.approval.requestApproval(pkg)
      } catch (error) {
        log.warn(`errored out: ${error}`)
        conn.write(JSON.stringify({
          op: 'APPROVAL',
          status: false,
          error: error.toString(),
        }))
        return
      }

      log.verbose(`giving response of ${approved}`)
      conn.write(JSON.stringify({
        op: 'APPROVAL',
        status: approved,
      }))
    })
  }

  boot () {
    log.verbose('mni is booting! no fear!')

    this.server.boot()
    this.approval.boot()
  }
}
