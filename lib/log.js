const levels = ['debug', 'verbose', 'info', 'notice', 'warn', 'error']

for (const level of levels) {
  exports[level] = (...args) => {
    console.log(`[${new Date().toISOString()}] [${level.toUpperCase()}]`, ...args)
  }
}
