const net = require('net')
const EventEmitter = require('events')

const log = require('./log')

module.exports = class Server extends EventEmitter {
  constructor (port) {
    super()
    this.port = port
    this.server = null
  }

  handlePacket (conn, packet) {
    const { action } = packet

    if (action == null || action !== 'REQUEST_INSTALL') {
      return
    }

    const { package: packageName } = packet
    if (!packageName) {
      return
    }

    log.info(`emitting package install request (${packageName})`)
    this.emit('installRequest', conn, packageName)
  }

  handleClient (conn) {
    log.verbose('received connection')

    conn.on('end', () => {
      log.verbose('client disconnected')
    })

    conn.on('data', (data) => {
      try {
        const packet = JSON.parse(data)
        this.handlePacket(conn, packet)
      } catch (err) {
        log.warn(`client submitted invalid json (${err.message})`)
      }
    })
  }

  boot () {
    log.verbose('booting tcp server')

    const server = net.createServer(
      this.handleClient.bind(this)
    )

    server.on('error', (err) => {
      throw err
    })

    log.info(`now listening on :${this.port}`)
    server.listen(this.port)
  }
}
