# mni

community approved npm install

## motivation

tired of your peers judging you for using certain npm packages? no fear!
install and configure mni to let your friends decide which npm packages you get
to install, directly from discord!

nobody asked for this.

## usage

```sh
# clone repo
$ git clone https://gitlab.com/slcx/mni.git
$ cd mni

# setup
$ cp config.example.json config.json
$ toaster config.json # configure

# link the package for global usage
$ npm ln | yarn link

# run server in background
$ mni-server

# no fear!

$ mni react
$ mni vue
$ mni <package>
```
